## Usage Example

Let's use [https://help.flcourts.org/](https://help.flcourts.org/) as an example usage.

### Crawler Handler

First of all we need to implement [the crawler handler](reference.md#crawler-handler) for [https://help.flcourts.org/](https://help.flcourts.org/). Let's start with minimal required configurations:

- Unique identifier
- The main site domain.

To do so please create `src/ContentImport/HelpFlcourts/Crawler.php`:

```php
<?php

namespace App\ContentImport\HelpFlcourts;

use ContextualCode\Crawler\Service\Handler as BaseCrawlerHandler;

class Crawler extends BaseCrawlerHandler
{
    public function getImportIdentifier(): string
    {
        return 'help-flcourts';
    }

    public function getDomain(): string
    {
        return 'help.flcourts.org';
    }
}
```

[https://help.flcourts.org/](https://help.flcourts.org/) uses insecure SSL certificate, so SSL certificate check should be disabled in the crawler:

```php
    ...
    public function verifySsl() :bool
    {
        return false;
    }
```

Also, some links are using `http` protocols but other ones are using `https` (which is whitelisted by default). We could whitelist `http` protocol, but in this case, resulting links would be the mess of `http` and `https` links. Instead of doing it, let's convert all `http` links to `https`:

```php
    ...
    public function fixUrl(string $url): string
    {
        $tmp = parse_url($url);
        if (
            isset($tmp['scheme'], $tmp['host'])
            && $tmp['scheme'] === 'http'
            && $tmp['host'] === 'help.flcourts.org'
        ) {
            $tmp['scheme'] = 'https';

            return http_build_url($tmp);
        }

        return $url;
    }
```

### Running the crawler

After the crawler handler is defined we are fine to run the crawler by using [`crawler:run`](reference.md#crawlerrun) command:

```bash
php bin/console crawler:run help-flcourts

 To get live logs, please run the following command in a new terminal:
 tail -f /XXX/var/log/contextualcode-crawler.log

Running the crawler ...
=======================

Url: https://help.flcourts.org/wp-content/uploads/youtube-1.png
Referer: https://help.flcourts.org/what-happens-in-court/

103/103 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%  1 min/1 min

All links are processed:
 * 102 valid links
 * 1 invalid links
```

### Tuning the crawler handler

There was 1 invalid link after the first crawler run. Lets get more details about it using [`crawler:get-invalid-pages`](reference.md#crawlerget-invalid-pages) command:

```bash
$ php bin/console crawler:get-invalid-pages help-flcourts
 ------------------------------------------------------------------- ------------------------- -------------------------------------------------- 
  URL                                                                 Error message             Referer                                           
 ------------------------------------------------------------------- ------------------------- -------------------------------------------------- 
  https://help.flcourts.org/https:///help.flcourts.org/legal-terms/   HTTP status code is 404   https://help.flcourts.org/what-happens-in-court/  
 ------------------------------------------------------------------- ------------------------- -------------------------------------------------- 
```

After looking closer at referer page (https://help.flcourts.org/what-happens-in-court/) sources, we can find an invalid link there (single slash between the protocol and domain):
```html
... <a href="https:/help.flcourts.org/legal-terms/#counterpetition">counterpetition</a>  ...
```

Let's fix this link in the crawler handler:
```php
    ...

    public function fixUrl(string $url): string
    {
        if ($url === 'https:/help.flcourts.org/legal-terms/#counterpetition') {
            return str_replace('https:/', 'https://', $url);
        }

        ...
    }
```

The final handler crawler handler will be:
```php
<?php

namespace App\ContentImport\HelpFlcourts;

use ContextualCode\Crawler\Service\Handler as BaseCrawlerHandler;

class Crawler extends BaseCrawlerHandler
{
    public function getImportIdentifier(): string
    {
        return 'help-flcourts';
    }

    public function getDomain(): string
    {
        return 'help.flcourts.org';
    }

    public function verifySsl() :bool
    {
        return false;
    }

    public function fixUrl(string $url): string
    {
        if ($url === 'https:/help.flcourts.org/legal-terms/#counterpetition') {
            return str_replace('https:/', 'https://', $url);
        }

        $tmp = parse_url($url);
        if (
            isset($tmp['scheme'], $tmp['host'])
            && $tmp['scheme'] === 'http'
            && $tmp['host'] === 'help.flcourts.org'
        ) {
            $tmp['scheme'] = 'https';

            return http_build_url($tmp);
        }

        return $url;
    }
}
```

Run the crawler one more time by using [`crawler:run`](reference.md#crawlerrun) command and providing `--purge` option:

```bash
php bin/console crawler:run help-flcourts --purge

 To get live logs, please run the following command in a new terminal:
 tail -f /XXX/var/log/contextualcode-crawler.log

Running the crawler ...
=======================

Url: https://help.flcourts.org/wp-content/uploads/youtube-1.png
Referer: https://help.flcourts.org/what-happens-in-court/

102/102 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%  2 min/2 min

All links are processed:
 * 102 valid links
 * 0 invalid links
```

This time there will be no invalid links, and the crawler results could be reused in the content import/any other functionality.
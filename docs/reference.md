## Crawler Handler

Each crawler handler is a PHP class which extends [`ContextualCode\Crawler\Service\Handler`](../src/Service/Handler.php). And it should implement at least two methods:

- `getImportIdentifier()` - unique string identifier
- `getDomain()` - the main domain of the crawled site

Also, there are some other methods which define the crawler behavior. And any of them could be overridden in the crawler handler, depending on the requirements:

|Method|Description|Default Value|
|---|---|---|
|`getProtocol`|The main protocol used for all pages.|`https`|
|`verifySsl`|Defines if SSL certificate should be checked during the page request.|`true`|
|`getHomepage`|Path to the home page from which crawler starts.|`/`|
|`getExtraPages`|Additional pages which should be crawled.|`[]`|
|`getUserAgent`|Crawlers User-Agent|`Contextual Code / Import Crawler`|
|`followRedirects`|Defines if the crawler should follow the redirects or not.|`true`|
|`getConnectionTimeout`|The number of seconds to wait while trying to connect.|`20`|
|`getRequestTimeout`|The maximum number of seconds to allow for the request.|`60`|
|`delayBetweenRequests`|The number of seconds to wait after each request.|`0`|
|`getPageContentTypesToStoreResponse`|Specifies for which pages the response is stored.|`['text/html', 'application/xhtml+xml']`|
|`getPageWithLinksContentTypes`|Defines from which pages the child links should be extracted.|`['text/html', 'application/xhtml+xml']`|
|`getPageLinksXpathSelectors`|Defines the XPath selector for a links to other pages.|`['//a[@href]', '//img[@src]']`|
|`getPageLinksUrlAttributes`|Specifies the attributes used to extract a link from different DOM elements.|`['a' => ['href'], 'img' => ['src']]`|
|`getPageLinksIncludeRegularExpressions`|The list of regular expressions. If any is specified, all the links which do not satisfy any of it will be ignored.|`[]`|
|`getPageLinksSkipRegularExpressions`|The list of regular expressions. If any links satisfy any of the specified regular expressions it will be ignored.|`[]`|
|`getWhitelistedDomains`|The list of the additional domains (in addition to the main one) which links should be crawled.|`[]`|
|`getWhitelistedProtocols`|The list of the additional protocols (in addition to the main one) which links should be crawled.|`[]`|
|`fixUrl(string $url)`|Callback function to convert the link. Might be useful for fixing the wrong links in on the source pages.|`$url`|
|`customizeRequest($curl)`|Callback function to set custom CURL options for the request. Might be useful to force HEAD requests for big files.|`void`|
|`extractAdditionalURLs(DOMXPath $response)`|Callback function to extract additional URLs. Might be useful when additional URLs are not rendered as HTML and stored as JSON in some attributes.|`[]`|


## Commands

This package provides a few commands. All of them expect to receive the handler identifier as the only argument. But some of them have optional extra parameters.

### `crawler:run`

This is the main command which starts the crawler. And it has 2 options:

- `--purge` if this option is passed, all the previous results for the specified crawler handler will be removed.
It is useful to specify this option when some previously crawled pages are not available anymore.

- `--resume` this option is useful for the cases when the command was interrupted for some reason. If this option is passed no previous results will be reset.

Example usage:
```bash
php bin/console crawler:run unique-identifier

 To get live logs, please run the following command in a new terminal:
 tail -f /XXX/var/log/contextualcode-crawler.log

Running the crawler ...
=======================

Url: http://www.site-to-crawl.com/
Referer:

282/2828 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100% 7 secs/7 secs

All links are processed:
 * 281 valid links
 * 1 invalid links
```

### `crawler:get-pages-info`

This command list crawled pages info.  You can pass `--limit` and `--offset` options.


Example usage:
```bash
php bin/console crawler:get-pages-info atj

 ------- ---------- ----------------- ------------- ------------------------------------------------------------------------------------------------------------------------------------------------ 
  Valid   Redirect   MIME Type         Status Code   URL                                                                                                                                             
 ------- ---------- ----------------- ------------- ------------------------------------------------------------------------------------------------------------------------------------------------ 
  1                  text/html         200           https://atj.flcourts.org/                                                                                                                       
  1                  text/html         200           https://atj.flcourts.org/about-us/                                                                                                              
  1                  text/html         200           https://atj.flcourts.org/administrative-orders/  
...
```

### `crawler:get-invalid-pages`

This command output the list of invalid pages. The page is marked as invalid when its response status code is not 200 or any other Curl error happened while it was requested. 

Example usage:
```bash
php bin/console crawler:get-invalid-pages school-justice

 --------------------------------------------------------- ------------------------- --------------------------------------------------------------------- 
  URL                                                       Error message             Referer                                                              
 --------------------------------------------------------- ------------------------- --------------------------------------------------------------------- 
  http://www.schooljustice.org/news/team-packets/broward/   HTTP status code is 404   http://www.schooljustice.org/news/november-16-17-2015/team-packets/  
 --------------------------------------------------------- ------------------------- --------------------------------------------------------------------- 
```

### `crawler:get-mime-types`

This command outputs all the possible mime types for the crawled pages with total numbers. And it might be very useful for analyzing the crawled data.

Example usage:

```bash
$ php bin/console crawler:get-mime-types help-flcourts

 ----------------- ----------------- 
  MIME Type         Number of pages  
 ----------------- ----------------- 
  image/png         59               
  text/html         35               
  application/pdf   4                
  image/jpeg        3                
  image/gif         1                
 ----------------- ----------------- 
```


## Page metadata

All the crawled data is stored as [`ContextualCode\Crawler\Entity\Page`](../src/Entity/Page.php) entities. And it has following properties:

|Property|Description|
|---|---|
|`url`|Page URL|
|`referer`|Referer page URL|
|`refererPath`|DOM XPath of the element on the referer page which pointed to the current page|
|`contentType`|MIME content type|
|`statusCode`|Response HTTP status code|
|`headers`|Array of response headers|
|`response`|The body of the response (it is not stored for the binary files)|
|`isRedirect`|`1` if the page is redirect and `0` in all other cases|
|`redirectUrl`|Final destination URL for redirect pages|
|`isValid`|`1` if the page is valid and `0` in all other cases|
|`errorMessage`|Description of the error for invalid pages|

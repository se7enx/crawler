<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\Command;

use ContextualCode\Crawler\Repository\PageRepository;
use ContextualCode\Crawler\Service\Messages;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GetInvalidPages extends Command
{
    protected static $defaultName = 'crawler:get-invalid-pages';

    private const ARGUMENT_IDENTIFIER = 'identifier';

    /** @var Messages */
    private $messages;

    /** @var PageRepository */
    private $pageRepository;

    public function __construct(Messages $messages, PageRepository $pageRepository)
    {
        $this->messages = $messages;
        $this->pageRepository = $pageRepository;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                self::ARGUMENT_IDENTIFIER,
                InputArgument::REQUIRED,
                $this->messages->get('command_argument_identifier')
            )
            ->setDescription($this->messages->get('command_invalid_links_description'));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $criteria = [
            'identifier' => $input->getArgument(self::ARGUMENT_IDENTIFIER),
            'isValid' => false,
        ];
        $pages = $this->pageRepository->findBy($criteria);

        $data = [];
        foreach ($pages as $page) {
            $data[] = [
                $page->getUrl(),
                $page->getErrorMessage(),
                $page->getReferer(),
            ];
        }

        $headers = [
            $this->messages->get('page_url'),
            $this->messages->get('page_error'),
            $this->messages->get('page_referer'),
        ];
        $io->table($headers, $data);

        return 0;
    }
}

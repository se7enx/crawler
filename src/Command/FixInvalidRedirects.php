<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\Command;

use ContextualCode\Crawler\Entity\Page;
use ContextualCode\Crawler\Exception\InvalidLink;
use ContextualCode\Crawler\Helper\Link;
use ContextualCode\Crawler\Repository\PageRepository;
use ContextualCode\Crawler\Service\Crawler;
use ContextualCode\Crawler\Service\Messages;
use PDO;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FixInvalidRedirects extends Command
{
    protected static $defaultName = 'crawler:fix-invalid-redirects';

    private const ARGUMENT_IDENTIFIER = 'identifier';

    /** @var Messages */
    private $messages;

    /** @var Crawler */
    private $crawler;

    /** @var PageRepository */
    private $pageRepository;

    public function __construct(
        Messages $messages,
        Crawler $crawler,
        PageRepository $pageRepository
    ) {
        $this->messages = $messages;
        $this->crawler = $crawler;
        $this->pageRepository = $pageRepository;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                self::ARGUMENT_IDENTIFIER,
                InputArgument::REQUIRED,
                $this->messages->get('command_argument_identifier')
            )
            ->setDescription($this->messages->get('command_invalid_redirects_description'));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $identifier = $input->getArgument(self::ARGUMENT_IDENTIFIER);
        try {
            $this->crawler->setHandler($identifier);
        } catch (InvalidArgumentException $e) {
            $io->error($e->getMessage());

            return 1;
        }

        $pages = $this->fetchPages();

        $io->newLine();
        $progressBar = $io->createProgressBar(count($pages));
        $progressBar->setFormat('debug');
        $progressBar->start();

        $info = $this->fixRedirects($pages, $progressBar);
        $progressBar->finish();
        $io->newLine(2);

        $message = $this->messages->get('command_invalid_redirects_success', $info);
        $io->writeln('');
        $io->block($message, null, 'fg=black;bg=green', ' ', true);

        return 0;
    }

    private function fetchPages(): iterable
    {
        return $this->pageRepository->createQueryBuilder('r')
            ->where(
                'r.identifier = :identifier',
                'r.statusCode = :ok_status',
                'r.isRedirect = :is_redirect',
                'p.url IS NULL'
            )
            ->leftJoin(Page::class, 'p', 'WITH', 'p.url = r.redirectUrl')
            ->setParameter('identifier', $this->crawler->getHandler()->getImportIdentifier())
            ->setParameter('ok_status', 200, PDO::PARAM_INT)
            ->setParameter('is_redirect', 1, PDO::PARAM_INT)
            ->getQuery()->execute();
    }

    private function fixRedirects(iterable $pages, ProgressBar $progressBar): array
    {
        $info = ['converted' => 0, 'skipped' => 0];
        $handler = $this->crawler->getHandler();
        foreach ($pages as $page) {
            $link = new Link($page->getRedirectUrl(), $page->getReferer());

            try {
                $link->validateExpressions(
                    $handler->getPageLinksIncludeRegularExpressions(),
                    $handler->getPageLinksSkipRegularExpressions()
                );
            } catch (InvalidLink $e) {
                $info['skipped']++;

                continue;
            }

            $page->setRedirect(false);
            $this->pageRepository->store($page);

            $info['converted']++;

            $progressBar->advance();
        }

        return $info;
    }
}

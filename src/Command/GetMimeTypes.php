<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\Command;

use ContextualCode\Crawler\Repository\PageRepository;
use ContextualCode\Crawler\Service\Messages;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GetMimeTypes extends Command
{
    protected static $defaultName = 'crawler:get-mime-types';

    private const ARGUMENT_IDENTIFIER = 'identifier';

    /** @var Messages */
    private $messages;

    /** @var PageRepository */
    private $pageRepository;

    public function __construct(Messages $messages, PageRepository $pageRepository)
    {
        $this->messages = $messages;
        $this->pageRepository = $pageRepository;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                self::ARGUMENT_IDENTIFIER,
                InputArgument::REQUIRED,
                $this->messages->get('command_argument_identifier')
            )
            ->setDescription($this->messages->get('command_mime_types_description'));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $identifier = $input->getArgument(self::ARGUMENT_IDENTIFIER);
        $data = $this->pageRepository->getMimeTypes($identifier);

        $headers = [
            $this->messages->get('page_type'),
            $this->messages->get('pages_count'),
        ];
        $io->table($headers, $data);

        return 0;
    }
}

<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200622180401 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cc_content_import_pages (identifier VARCHAR(32) NOT NULL, url VARCHAR(736) NOT NULL, referer VARCHAR(736) DEFAULT NULL, referer_path VARCHAR(736) DEFAULT NULL, content_type VARCHAR(255) DEFAULT NULL, status_code SMALLINT DEFAULT 200, error_message VARCHAR(255) DEFAULT NULL, redirect_url VARCHAR(736) DEFAULT NULL, headers JSON DEFAULT NULL, response LONGTEXT DEFAULT NULL, is_valid TINYINT(1) DEFAULT 0, is_processed TINYINT(1) DEFAULT 0, is_redirect TINYINT(1) DEFAULT 0, page_order INT DEFAULT 0 NOT NULL, INDEX valid_idx (is_valid), INDEX processed_idx (is_processed), PRIMARY KEY(identifier, url)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_520_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE cc_content_import_pages');
    }
}

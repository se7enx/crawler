<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\Exception;

use Exception;

class InvalidLink extends Exception
{
}

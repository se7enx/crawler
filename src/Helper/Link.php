<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\Helper;

use ContextualCode\Crawler\Exception\InvalidLink;

class Link
{
    private $url;
    private $referer;

    public function __construct(string $url, ?string $referer = null)
    {
        $this->url = $url;
        $this->referer = $referer;
    }

    public static function encodeUrl(string $url): string
    {
        $parts = parse_url($url);
        if (!empty($parts['path'])) {
            $parts['path'] = implode('/', array_map('rawurlencode', explode('/', $parts['path'])));
        }

        return http_build_url($parts);
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function validate(array $hosts, array $protocols): void
    {
        if ($this->isEmpty()) {
            throw new InvalidLink('Link is empty');
        }

        if ($this->isHashOnly()) {
            throw new InvalidLink('Link contains only the fragment (hash)');
        }

        if (!$this->isHostWhitelisted($hosts)) {
            throw new InvalidLink('Invalid host');
        }

        if (!$this->isProtocolWhitelisted($protocols)) {
            throw new InvalidLink('Invalid protocol');
        }
    }

    public function validateExpressions(array $include, array $skip): void
    {
        if (!$this->isMatchingAnyIncludeRegularExpression($include)) {
            throw new InvalidLink('Does not match any include regular expression');
        }

        $matched = $this->getMatchingSkipRegularExpression($skip);
        if (null !== $matched) {
            throw new InvalidLink(sprintf('Matches "%s" skip regular expression', $matched));
        }
    }

    public function fixUrl(string $protocol, string $domain): void
    {
        $this->trim();
        $this->fixProtocol($protocol);
        $this->convertRelative();
        $this->removeDoubleSlash();
        $this->removeHash();
        $this->fixRelativeParts();
        $this->fixGetParameters();
        $this->forceAbsolute($protocol, $domain);
    }

    public function isHashOnly(): bool
    {
        return strpos($this->url, '#') === 0;
    }

    public function forceAbsolute(string $protocol, string $domain): void
    {
        $info = parse_url($this->url);

        if (!isset($info['scheme']) && !isset($info['host'])) {
            $this->url = $protocol . '://' . $domain . '/' . ltrim($this->url, '/');
        }
    }

    private function isEmpty(): bool
    {
        return $this->url === '';
    }

    private function isHostWhitelisted(array $hosts): bool
    {
        $info = parse_url($this->url);

        if (!isset($info['host'])) {
            return true;
        }

        return in_array($info['host'], $hosts, true);
    }

    private function isProtocolWhitelisted(array $protocols): bool
    {
        $info = parse_url($this->url);

        if (!isset($info['scheme'])) {
            return true;
        }

        return in_array($info['scheme'], $protocols, true);
    }

    private function isMatchingAnyIncludeRegularExpression(array $expressions): bool
    {
        if (!count($expressions)) {
            return true;
        }

        foreach ($expressions as $expression) {
            if (preg_match($expression, $this->url) === 1) {
                return true;
            }
        }

        return false;
    }

    private function getMatchingSkipRegularExpression(array $expressions): ?string
    {
        foreach ($expressions as $expression) {
            if (preg_match($expression, $this->url) === 1) {
                return $expression;
            }
        }

        return null;
    }

    private function fixProtocol(string $protocol): void
    {
        if (strpos($this->url, '//') === 0) {
            $this->url = $protocol . ':' . $this->url;
        }
    }

    private function convertRelative(): void
    {
        $info = parse_url($this->url);

        if (
            !isset($info['scheme'])
            && !isset($info['host'])
            && strpos($this->url, '/') !== 0
        ) {
            $this->url = $this->convertRelativeToAbsolute($this->url, $this->referer);
        }
    }

    /** @link https://stackoverflow.com/a/4444490/5494509 */
    private function convertRelativeToAbsolute(string $rel, string $base): string
    {
        /* queries and anchors */
        if ($rel[0] === '#' || $rel[0] === '?') {
            return $base . $rel;
        }

        //

        $vars = parse_url($base);
        if (!isset($vars['path'])) {
            $vars['path'] = '';
        }

        $path = preg_replace('#/[^/]*$#', '', $vars['path']);
        $abs = $vars['host'] . $path . '/' . $rel;

        /* replace '//' or '/./' or '/foo/../' with '/' */
        $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
        for ($n = 1; $n > 0; $abs = preg_replace($re, '/', $abs, -1, $n)) {
        }

        return $vars['scheme'] . '://' . $abs;
    }

    private function fixRelativeParts(): void
    {
        $this->url = str_replace('/./', '/', $this->url);

        if (strpos($this->url, '..') !== false) {
            $parts = explode('/', $this->url);
            while (in_array('..', $parts, true)) {
                foreach ($parts as $k => $part) {
                    if ($part === '..') {
                        if (isset($parts[$k - 1])) {
                            unset($parts[$k - 1]);
                        }
                        unset($parts[$k]);
                        $parts = array_values($parts);
                        break;
                    }
                }
            }
            $this->url = implode('/', $parts);
        }
    }

    private function removeHash(): void
    {
        $this->url = preg_replace('/#.*/', '', $this->url);
    }

    private function removeDoubleSlash(): void
    {
        $parts = parse_url($this->url);
        if (!empty($parts['path'])) {
            $parts['path'] = str_replace('//', '/', $parts['path']);
        }

        $this->url = http_build_url($parts);
    }

    private function trim(): void
    {
        $this->url = str_replace("\r", '', $this->url);
        $this->url = trim($this->url);
    }

    private function fixGetParameters(): void
    {
        $parts = explode('?', $this->url);
        if (count($parts) > 2) {
            $this->url = $parts[0] . '?' . $parts[count($parts) - 1];
        }
    }
}

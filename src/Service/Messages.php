<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\Service;

use RuntimeException;

class Messages
{
    private $messages = [
        'command_run_description' => 'Runs the website crawler using provided handler',
        'command_invalid_links_description' => 'Outputs the list of invalid pages',
        'command_mime_types_description' => 'Outputs possible mime types for crawled pages',
        'command_invalid_redirects_description' => 'Converts redirects without valid destination URL into the regular non-redirect pages',
        'command_argument_identifier' => 'The crawler identifier, needs to be defined in your app',
        'command_option_limit' => 'Limit',
        'command_option_offset' => 'Offset',
        'command_option_purge' => 'All the pages from previous runs will be removed if this option is provided',
        'command_option_resume' => 'This option is used to resume previously non-finished crawler',
        'command_live_logs' => 'To get live logs, please run the following command in a new terminal:

tail -f %s',
        'command_run_title' => 'Running the crawler ...',
        'command_run_success' => 'All links are processed:

 * %d valid links
 * %d invalid links',
        'command_invalid_redirects_success' => 'Redirects without valid destination were updated:

 * %d converted to regular non-redirect pages
 * %d untouched as they point to external pages',
        'page_url' => 'URL',
        'page_status_code' => 'Status Code',
        'page_valid' => 'Valid',
        'page_redirect' => 'Redirect',
        'page_error' => 'Error message',
        'page_referer' => 'Referer',
        'error_no_available_handlers' => 'No available crawler handlers were found. Please implement them.',
        'page_type' => 'MIME Type',
        'pages_count' => 'Number of pages',
        'error_invalid_handler' => '"%s" is invalid handler. Available handlers: "%s"',
        'unable_get_xpath' => 'Unable to get page XPath',
        'pages_extracted' => 'Extracted %d pages',
        'pages_added' => 'Added %d pages',
        'additional_pages_added' => 'Added %d additional pages',
        'link_skipped' => 'Link skipped, reason: %s',
        'unable_extract_url' => 'Unable to extract URL from "%s" element (%s)',
        'request_sent' => 'Request sent',
    ];

    public function get(string $identifier, array $params = []): string
    {
        if (!isset($this->messages[$identifier])) {
            throw new RuntimeException(sprintf('Unable to find "%s" message', $identifier));
        }

        $message = $this->messages[$identifier];

        return count($params) ? vsprintf($message, $params) : $message;
    }
}

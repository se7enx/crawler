<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\DependencyInjection;

use ContextualCode\Crawler\Service\Handler as CrawlerHandler;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Yaml\Yaml;

class CrawlerExtension extends Extension implements PrependExtensionInterface
{
    public const TAG_HANDLER = 'contextualcode.crawler.handler';

    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $container->registerForAutoconfiguration(CrawlerHandler::class)
            ->addTag(self::TAG_HANDLER);
    }

    public function prepend(ContainerBuilder $container): void
    {
        $this->prependExtension($container, 'monolog');
    }

    protected function prependExtension(
        ContainerBuilder $container,
        string $extension,
        ?string $configFileName = null
    ): void {
        if ($configFileName === null) {
            $configFileName = $extension;
        }

        $configFile = __DIR__ . '/../Resources/config/' . $configFileName . '.yaml';
        $config = Yaml::parseFile($configFile);
        $container->prependExtensionConfig($extension, $config);
        $container->addResource(new FileResource($configFile));
    }
}
